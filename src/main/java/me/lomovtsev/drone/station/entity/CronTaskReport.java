package me.lomovtsev.drone.station.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CronTaskReport {
    private Long id;
    private Long timestamp;
    private String report;
}
