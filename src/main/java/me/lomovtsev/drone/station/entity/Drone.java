package me.lomovtsev.drone.station.entity;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Drone {
    private Long id;
    private String serialNumber;
    private Model model;
    private int weightLimit; // gramms
    private int batteryCapacity; // in percents;
    private State state;

    public enum Model {
        LIGHTWEIGHT, MIDDLEWEIGHT, CRUISERWEIGHT, HEAVYWEIGHT;
    }

    public enum State {
        IDLE, LOADING, LOADED, DELIVERING, DELIVERED, RETURNING;
    }

    @Data
    public static class CurrentWeight {
        private Long id;
        private int weight;

        public static CurrentWeight EMPTY_CONTENT = new CurrentWeight();
    }
}

