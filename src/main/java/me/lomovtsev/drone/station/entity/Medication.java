package me.lomovtsev.drone.station.entity;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Medication {
    private Long id;
    private String name;
    private int weight;
    private String code;
    private String image; // base64 or mb the link to the entity or url  TODO: think about it
}
