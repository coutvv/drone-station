package me.lomovtsev.drone.station.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.Collections;
import java.util.Map;

@ControllerAdvice
public class GlobalRestExceptionHandler {

    @ExceptionHandler({DroneStationException.class})
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public Map<String,String> customizeResponse(DroneStationException e){
        return Collections.singletonMap("message",e.getMessage());
    }

    @ExceptionHandler({NotFoundException.class})
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public Map<String,String> customizeResponse(NotFoundException e){
        return Collections.singletonMap("message",e.getMessage());
    }
}
