package me.lomovtsev.drone.station.exceptions;

public class DroneStationException extends RuntimeException {
    public DroneStationException(String message) {
        super(message);
    }
}
