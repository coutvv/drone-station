package me.lomovtsev.drone.station.repository.mapper;

import me.lomovtsev.drone.station.entity.Medication;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface MedicationMapper {

    @Insert("INSERT INTO medication (name, weight, code, image)" +
            "VALUES (#{name}, #{weight}, #{code}, #{image})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(Medication medication);

    @Select("SELECT id, name, weight, code, image FROM medication")
    List<Medication> getAll();

    @Select({
            "<script>",
            "SELECT id, name, weight, code, image FROM medication ",
            "WHERE id in (",
            "<foreach item='med_id' collection='ids' open='' separator=',' close=''>" +
                    "#{med_id}" +
                    "</foreach>",
            ")",
            "</script>"
    })
    List<Medication> getAllByIds(List<Long> ids);
}
