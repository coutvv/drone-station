package me.lomovtsev.drone.station.repository.mapper;

import me.lomovtsev.drone.station.entity.CronTaskReport;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;

@Mapper
public interface CronTaskReportMapper {


    @Insert("INSERT INTO cron_reports (timestamp, report)" +
            "VALUES (#{timestamp}, #{report})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(CronTaskReport report);
}
