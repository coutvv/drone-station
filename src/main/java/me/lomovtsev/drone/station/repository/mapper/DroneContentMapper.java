package me.lomovtsev.drone.station.repository.mapper;

import me.lomovtsev.drone.station.entity.Drone;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.MapKey;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface DroneContentMapper {

    @Select("SELECT medication_id FROM drone_content " +
            "WHERE drone_id = #{droneId}")
    List<Long> getDroneContent(long droneId);

    @Insert("INSERT INTO drone_content (drone_id, medication_id) " +
            "VALUES (#{droneId}, #{medicationId})")
    void loadDroneContent(long droneId, long medicationId);

    /**
     * @param droneId
     * @return sum of content's weight or null if it is empty
     */
    @Select("SELECT SUM(WEIGHT) FROM drone_content dc " +
            "LEFT JOIN medication m ON m.id = medication_id " +
            "WHERE drone_id = #{droneId};")
    Integer currentWeight(long droneId);

    @Select("SELECT drone_id as id, SUM(WEIGHT) as weight from drone_content dc " +
            "LEFT JOIN medication m ON m.id = medication_id " +
            "GROUP BY drone_id")
    @MapKey("id")
    Map<Long, Drone.CurrentWeight> currentWeightForAllDrones();

    @Insert({
            "<script>",
                "INSERT INTO drone_content",
                "(drone_id, medication_id)",
                "VALUES" +
                        "<foreach item='med_id' collection='medicationIds' open='' separator=',' close=''>" +
                            "(#{droneId}, #{med_id})" +
                        "</foreach>",
            "</script>"
    })
    void loadDrone(long droneId, List<Long> medicationIds);
}
