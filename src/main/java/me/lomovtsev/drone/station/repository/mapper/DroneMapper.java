package me.lomovtsev.drone.station.repository.mapper;

import me.lomovtsev.drone.station.entity.Drone;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface DroneMapper {
    @Insert("INSERT INTO drone (serial_number, state, model, weight_limit, battery_capacity)" +
            "VALUES (#{serialNumber}, #{state}, #{model}, #{weightLimit}, #{batteryCapacity})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(Drone drone);

    @Select("SELECT id, serial_number, model, weight_limit, battery_capacity, state FROM drone")
    List<Drone> getAll();

    @Select("SELECT id, serial_number, model, weight_limit, battery_capacity, state FROM drone " +
            "WHERE id = #{droneId}")
    Drone getDrone(long droneId);

    @Update("UPDATE drone " +
            "SET state = #{state} " +
            "WHERE id = #{droneId}")
    void updateDroneState(long droneId, Drone.State state);
}
