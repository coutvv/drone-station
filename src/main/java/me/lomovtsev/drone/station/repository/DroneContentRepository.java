package me.lomovtsev.drone.station.repository;

import me.lomovtsev.drone.station.entity.Medication;
import me.lomovtsev.drone.station.repository.mapper.DroneContentMapper;
import me.lomovtsev.drone.station.repository.mapper.DroneMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public class DroneContentRepository {
    @Autowired
    private DroneMapper droneMapper;

    @Autowired
    private DroneContentMapper droneContentMapper;

    @Transactional
    public boolean loadDrone(long droneId, List<Medication> medications) {
        var drone = droneMapper.getDrone(droneId);
        if (drone == null) {
            return false;
        }
        var currentWeight = droneContentMapper.currentWeight(droneId);
        var freeSpace = drone.getWeightLimit() - (currentWeight == null ? 0 : currentWeight);
        var addedWeight = medications.stream().mapToInt(Medication::getWeight).sum();
        if (freeSpace < addedWeight) {
            return false;
        } else {
            droneContentMapper.loadDrone(droneId, medications.stream().map(Medication::getId).toList());
            return true;
        }
    }

}
