package me.lomovtsev.drone.station.cron;

import lombok.extern.slf4j.Slf4j;
import me.lomovtsev.drone.station.entity.CronTaskReport;
import me.lomovtsev.drone.station.repository.mapper.CronTaskReportMapper;
import me.lomovtsev.drone.station.repository.mapper.DroneMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class BatteryReportTask {

    @Autowired
    private DroneMapper droneMapper;

    @Autowired
    private CronTaskReportMapper cronTaskReportMapper;

    @Scheduled(fixedRateString = "${check.battery.rate.ms}", initialDelayString = "${check.battery.delay.ms}")
    public void processing() {
        log.info("Battery report task has started");
        var drones = droneMapper.getAll();
        var reportValue = new StringBuilder("BatteryReport\n");
        reportValue.append("id \t|\t serial number \t|\t battery value\n");
        drones.forEach(drone -> {
            reportValue.append(drone.getId()).append("\t|\t")
                    .append(drone.getSerialNumber()).append("\t|\t")
                    .append(drone.getBatteryCapacity()).append("\n");
        });
        var report = CronTaskReport.builder()
                        .timestamp(System.currentTimeMillis())
                        .report(reportValue.toString())
                        .build();
        cronTaskReportMapper.insert(report);
        log.info("Report for battery checking was processed. ID = " + report.getId());
    }
}
