package me.lomovtsev.drone.station.service;

import me.lomovtsev.drone.station.entity.Drone;
import me.lomovtsev.drone.station.order.DroneOrderLogger;
import me.lomovtsev.drone.station.repository.mapper.DroneMapper;
import me.lomovtsev.drone.station.rest.dto.DroneDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DroneService {
    @Autowired
    private DroneMapper droneMapper;

    @Autowired
    private DroneOrderLogger droneOrderLogger;

    public long registerNewDrone(DroneDTO drone) {
        Drone newDrone = Drone.builder()
                .serialNumber(drone.getSerialNumber())
                .state(Drone.State.IDLE)
                .model(drone.getModel())
                .weightLimit(drone.getWeightLimit())
                .batteryCapacity(drone.getBatteryCapacity())
                .build();
        droneMapper.insert(newDrone);
        droneOrderLogger.logDroneRegistry(newDrone);
        return newDrone.getId();
    }

    public List<Drone> getAllDrones() {
        return droneMapper.getAll();
    }

    public Drone getDrone(long id) {
        return droneMapper.getDrone(id);
    }

}
