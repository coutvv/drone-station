package me.lomovtsev.drone.station.service;

import me.lomovtsev.drone.station.entity.Drone;
import me.lomovtsev.drone.station.entity.Medication;
import me.lomovtsev.drone.station.exceptions.DroneStationException;
import me.lomovtsev.drone.station.exceptions.NotFoundException;
import me.lomovtsev.drone.station.order.DroneOrderLogger;
import me.lomovtsev.drone.station.repository.DroneContentRepository;
import me.lomovtsev.drone.station.repository.mapper.DroneContentMapper;
import me.lomovtsev.drone.station.repository.mapper.DroneMapper;
import me.lomovtsev.drone.station.repository.mapper.MedicationMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class DroneContentService {

    private static final int MIN_BATTERY_LEVEL_TO_LOAD = 25;

    @Autowired
    private DroneMapper droneMapper;

    @Autowired
    private MedicationMapper medicationMapper;

    @Autowired
    private DroneContentMapper droneContentMapper;

    @Autowired
    private DroneOrderLogger droneOrderLogger;

    @Autowired
    private DroneContentRepository droneContentRepository;

    public List<Long> getDroneContent(long droneId) {
        var drone = droneMapper.getDrone(droneId);
        if (drone != null) {
            return droneContentMapper.getDroneContent(droneId);
        } else {
            throw new NotFoundException("No drone with id = " + droneId);
        }
    }

    public void loadDrone(long droneId, List<Long> meds) {
        var drone = getDroneForLoad(droneId);
        var addedMedications = mapMedications(meds);
        var prevState = drone.getState();
        droneMapper.updateDroneState(droneId, Drone.State.LOADING);
        try {
            var loaded = droneContentRepository.loadDrone(droneId, addedMedications);
            if (loaded) {
                droneOrderLogger.logDroneLoadedMeds(drone, addedMedications);
            } else {
                throw new DroneStationException("Can't load meds to drone " + droneId);
            }
        } finally {
            droneMapper.updateDroneState(droneId, prevState);
        }
    }

    private Drone getDroneForLoad(long droneId) {
        var drone = droneMapper.getDrone(droneId);
        if (drone == null) {
            throw new NotFoundException("Incorrect drone Id: " + droneId);
        }
        if (drone.getBatteryCapacity() < MIN_BATTERY_LEVEL_TO_LOAD) {
            throw new DroneStationException(
                    "Can't load to drone: " + droneId +
                            " because it has low batter level: " + drone.getBatteryCapacity()
            );
        }
        if (drone.getState() == Drone.State.LOADING) {
            throw new DroneStationException(
                    "Drone with id " + droneId + " is already in loading state, can't load meds"
            );
        }
        return drone;
    }

    public List<Long> getAvailableDrones(List<Long> medicationIds) {
        var drones = droneMapper.getAll();// TODO: could be optimized
        var addedWeight = mapMedications(medicationIds).stream().mapToInt(Medication::getWeight).sum();
        var currentWeights = droneContentMapper.currentWeightForAllDrones();
        return drones.stream()
                .filter(drone -> {
                    var currentWeight = currentWeights.getOrDefault(drone.getId(), Drone.CurrentWeight.EMPTY_CONTENT)
                            .getWeight();
                    return drone.getWeightLimit() - currentWeight >= addedWeight;
                })
                .map(Drone::getId)
                .toList();
    }

    private List<Medication> mapMedications(List<Long> medIds) {
        var medications = medicationMapper.getAllByIds(medIds).stream()
                .collect(Collectors.toMap(Medication::getId, Function.identity()));
        var addedMedications = medIds.stream()
                .peek(medId -> {
                    if (!medications.containsKey(medId)) {
                        throw new DroneStationException("incorrect medId value: " + medId);
                    }
                })
                .map(medications::get)
                .toList();
        return addedMedications;
    }
}
