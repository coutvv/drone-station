package me.lomovtsev.drone.station.order;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DroneOrder {
    private Long id;
    private Long droneId;
    private Type type;
    private long date;
    private String author;
    private String message;

    public enum Type {
        CREATED, // register drone
        LOAD_MEDS, // load medication to drone
    }
}
