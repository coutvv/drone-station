package me.lomovtsev.drone.station.order;

import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface DroneOrderMapper {

    @Insert("INSERT INTO drone_orders (drone_id, type, date, author, message)" +
            "VALUES (#{droneId}, #{type}, #{date}, #{author}, #{message})")
    @Options(useGeneratedKeys = true, keyProperty = "id")
    void insert(DroneOrder droneOrder);

    @Select("SELECT id, drone_id, type, date, author, message FROM drone_orders")
    List<DroneOrder> getAll();
}
