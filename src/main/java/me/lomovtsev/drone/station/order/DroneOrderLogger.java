package me.lomovtsev.drone.station.order;

import me.lomovtsev.drone.station.entity.Drone;
import me.lomovtsev.drone.station.entity.Medication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class DroneOrderLogger {

    private static final String DEFAULT_AUTHOR = "ADMIN"; // TODO: should be replaced with permission module

    @Autowired
    private DroneOrderMapper droneOrderMapper;

    public void logDroneRegistry(Drone drone) {

        var order = DroneOrder.builder()
                .droneId(drone.getId())
                .type(DroneOrder.Type.CREATED)
                .date(System.currentTimeMillis())
                .author(DEFAULT_AUTHOR)
                .build();
        droneOrderMapper.insert(order);
    }

    public void logDroneLoadedMeds(Drone drone, List<Medication> medications) {
        var meds = medications.stream()
                .map(Medication::getId)
                .map(Object::toString)
                .collect(Collectors.joining(", "));
        var order = DroneOrder.builder()
                .droneId(drone.getId())
                .type(DroneOrder.Type.LOAD_MEDS)
                .date(System.currentTimeMillis())
                .message("Meds: " + meds)
                .author(DEFAULT_AUTHOR)
                .build();
        droneOrderMapper.insert(order);
    }
}
