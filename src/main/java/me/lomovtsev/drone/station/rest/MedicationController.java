package me.lomovtsev.drone.station.rest;

import me.lomovtsev.drone.station.entity.Medication;
import me.lomovtsev.drone.station.repository.mapper.MedicationMapper;
import me.lomovtsev.drone.station.rest.dto.MedicationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
public class MedicationController {

    @Autowired
    private MedicationMapper medicationMapper;

    @PutMapping("/medications")
    public Long registryMedication(@RequestBody @Valid MedicationDTO medicationDto) {
        var medication = Medication.builder()
                .code(medicationDto.getCode())
                .name(medicationDto.getName())
                .image(medicationDto.getImage())
                .weight(medicationDto.getWeight())
                .build();
        medicationMapper.insert(medication);
        return medication.getId();
    }

    @GetMapping("/medications")
    public List<Medication> getAll() {
        return medicationMapper.getAll();
    }
}
