package me.lomovtsev.drone.station.rest;

import lombok.extern.slf4j.Slf4j;
import me.lomovtsev.drone.station.entity.Drone;
import me.lomovtsev.drone.station.rest.dto.DroneDTO;
import me.lomovtsev.drone.station.rest.dto.DroneStateDTO;
import me.lomovtsev.drone.station.service.DroneService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@Slf4j
public class DroneController {

    @Autowired
    private DroneService droneService;

    @GetMapping("/drones")
    public List<Drone> getAllDrones() {
        return droneService.getAllDrones();
    }

    @PutMapping("/drones")
    public Long register(@Valid @RequestBody DroneDTO drone) {
        return droneService.registerNewDrone(drone);
    }

    @GetMapping("/drones/{droneId}/state")
    public DroneStateDTO getDroneState(@PathVariable("droneId") long droneId) {
        var drone = droneService.getDrone(droneId);
        return DroneStateDTO.builder()
                .battery(drone.getBatteryCapacity())
                .build();
    }
}
