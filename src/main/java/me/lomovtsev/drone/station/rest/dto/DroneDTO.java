package me.lomovtsev.drone.station.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import me.lomovtsev.drone.station.entity.Drone;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
public class DroneDTO {

    @Size(min = 1, message = "too short serial number")
    @Size(max = 100, message = "incorrect size of drone's serial number")
    private String serialNumber;
    private Drone.Model model;

    @Min(1)
    @Max(500)
    private int weightLimit; // gramms

    @Min(0)
    @Max(100)
    private int batteryCapacity; // in percents;
}
