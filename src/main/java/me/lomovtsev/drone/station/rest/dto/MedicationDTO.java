package me.lomovtsev.drone.station.rest.dto;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

@Data
public class MedicationDTO {
    @Pattern(regexp = "^[a-zA-Z0-9_-]*")
    private String name;
    @Min(1)
    private int weight;
    @Pattern(regexp = "^[A-Z0-9_]*")
    private String code;
    private String image;
}
