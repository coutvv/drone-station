package me.lomovtsev.drone.station.rest;

import me.lomovtsev.drone.station.rest.dto.LoadMedDTO;
import me.lomovtsev.drone.station.service.DroneContentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class DroneLoadingController {

    @Autowired
    private DroneContentService droneContentService;

    @GetMapping("/drones/{droneId}/content")
    public List<Long> checkDroneContent(@PathVariable("droneId") long droneId) {
        return droneContentService.getDroneContent(droneId);
    }

    @PostMapping("/drones/{droneId}/content")
    public String loadMedication(
            @PathVariable("droneId") long droneId,
            @RequestBody LoadMedDTO loadMedDTO
    ) {
        droneContentService.loadDrone(droneId, loadMedDTO.getMedicationsId());
        return "Success";
    }

    @GetMapping("/drones/available")
    public List<Long> getAvailableDrones(@RequestParam("medId") List<Long> medIds) {
        return droneContentService.getAvailableDrones(medIds);
    }
}
