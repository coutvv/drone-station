package me.lomovtsev.drone.station.rest.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DroneStateDTO {
    private int battery;
}
