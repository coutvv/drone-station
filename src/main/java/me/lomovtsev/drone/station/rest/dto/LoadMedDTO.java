package me.lomovtsev.drone.station.rest.dto;

import lombok.Data;

import java.util.List;

@Data
public class LoadMedDTO {
    private List<Long> medicationsId;
}
