INSERT INTO drone (serial_number,model,state,battery_capacity,weight_limit) VALUES
	 ('ABCDEFG','LIGHTWEIGHT','IDLE',100,100),
	 ('ABCDEF1','MIDDLEWEIGHT','IDLE',89,200),
	 ('ABCDEF2','CRUISERWEIGHT','IDLE',78,350),
	 ('ABCDEF3','HEAVYWEIGHT','IDLE',66,500),
	 ('HouseMD','HEAVYWEIGHT','IDLE',17,500);

INSERT INTO drone_orders (drone_id,"type",date,author,message) VALUES
	 (1,'CREATED',1665399643040,'ADMIN',NULL),
	 (2,'CREATED',1665399662399,'ADMIN',NULL),
	 (3,'CREATED',1665399684206,'ADMIN',NULL),
	 (4,'CREATED',1665399701865,'ADMIN',NULL),
	 (5,'CREATED',1665400053117,'ADMIN',NULL);

INSERT INTO medication (name,code,weight,image) VALUES
	 ('Aspirin',111,2,'image'),
	 ('Interferon',222,16,'image'),
	 ('Vitamin C',333,32,'image'),
	 ('Steroids',444,64,'image'),
	 ('Chemical Therapy',555,128,'image'),
	 ('Cardiostimulator',666,256,'image'),
	 ('Vicodine',777,400,'image');
