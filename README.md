# Drone Station

## About

This is the solution for [Task](./TASK.md)

### Technology

- Maven for building/running
- Spring Boot
- SQLite as Data Storage (`./drone-station.db`)
- Liquibase for migration scripts
- MyBatis as persistence framework

### Prerequisites

Mandatory install for build/run:
- [OpenJDK 17](https://jdk.java.net/archive/) or upper

Optional:
- [curl](https://curl.se) for making http requests
- [DBeaver](https://dbeaver.io) for access data storage

### How to launch

For building application use in terminal:
```shell
./mvnw install
```

For running application use in terminal:
```shell
./mvnw spring-boot:run
```
After starting the app will created the file `drone-station.db` with tables and data. 
Remove it and restart application for reset data.

## Features

### Periodic task for creating battery level report

By default run in 1 minute after starting and then periodically once an hour.
It could be change in the properties at `src/main/resources/application.properties`:
- check.battery.rate.ms
- check.battery.delay.ms

The report saved at database in the `drone-station.db` file at table `cron_reports`.
It could be extracted in any SQL-client application via request:
```roomsql
SELECT * FROM cron_reports;
```

### Rest API

There is implemented endpoints for:
- registering a drone;
- loading a drone with medication items;
- checking loaded medication items for a given drone;
- checking available drones for loading;
- check drone battery level for a given drone;

### Examples of HTTP requests

List of all registered drones:
```shell
curl --location --request GET 'localhost:8080/drones'
```

List of all registered medications:
```shell
curl --location --request GET 'localhost:8080/medications'
```

Check battery level for drone with ID = 1:
```shell
curl --location --request GET 'localhost:8080/drones/1/state'
```

Check loaded medication items for drone with ID = 1:
```shell
curl --location --request GET 'localhost:8080/drones/1/content'
```

Get available drones for loading medicament items with ids: 1, 2, 3, 4, 5:
```shell
curl --location --request GET 'localhost:8080/drones/available?medId=1&medId=2&medId=3&medId=4&medId=5' \
--data-raw ''
```

Register new drone with serialNumber = `SERIALNUMBER`, model = `HEAVYWEIGHT`, weight limit = `500` 
and battery capacity = 100:
```shell
curl --location --request PUT 'localhost:8080/drones' \
--header 'Content-Type: application/json' \
--data-raw '{
    "serialNumber": "SERIALNUMBER",
    "model": "HEAVYWEIGHT",
    "weightLimit": 500,
    "batteryCapacity": 100
}'
```

Register new medicament item with name = `Vicodine_V2`, code = `VICOD22`, weight = `400` and image = `image` 
```shell
curl --location --request PUT 'localhost:8080/medications' \
--header 'Content-Type: application/json' \
--data-raw '{
    "name": "Vicodine_V2",
    "code": "VICOD22",
    "weight": 400,
    "image": "image"
}'
```

Load the drone (id = 2) with medicament items (id = 3, 2, 3):
```shell
curl --location --request POST 'localhost:8080/drones/2/content' \
--header 'Content-Type: application/json' \
--data-raw '{
    "medicationsId": [3,2,1]
}'
```




